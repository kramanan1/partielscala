package fr.upem.partiel.part2.model

import fr.upem.partiel.part2.model.Movie._

// TODO You have to create all the classes you need for the exam
// TODO Don't forget to read the existing code and the unit tests to get some clues !


// TODO Create this model
trait Movie{
  def getTitle : String
  def getDirector : String
  def getYear : Int
  def getViews : Long
  def getCountry : Country
}

object Movie {

  def apply(title: Title, director : Director, year : Year, views : Views, country: Country) : Movie = new Movie {
    override def getTitle: String = title.name

    override def getDirector: String = director.name

    override def getYear: Int = year.year

    override def getViews: Long = views.views

    override def getCountry: Country = country
  }

  // TODO Create this model
  trait Title{
    def name : String
  }

  // TODO Create this model
  trait Director{
    def name : String
  }

  // TODO Create this model
  trait Year{
    def year : Int
  }

  // TODO Create this model
  trait Views{
    def views : Long
  }

  trait Country

  object Country {

    final case object France extends Country

    final case object England extends Country

    final case object Italy extends Country

    final case object Germany extends Country

    final case object UnitedStates extends Country

  }

  // TODO Create this method
  def movie(title: Title, director: Director, year: Year, views: Views, country: Country): Movie = Movie(title, director, year, views, country)

  // TODO Create this method
  def title(s: String): Title = new Title {
    override def name: String = s
  }

  // TODO Create this method
  def director(fn: String, ln: String): Director = new Director {
    override def name: String = s"${fn} ${ln}"
  }

  // TODO Create this method
  def year(value: Int): Year = new Year {
    override def year: Int = value
  }

  // TODO Create this method
  def views(value: Long): Views = new Views {
    override def views: Long = value
  }

}