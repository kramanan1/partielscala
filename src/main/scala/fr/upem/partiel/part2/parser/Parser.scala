package fr.upem.partiel.part2.parser

import fr.upem.partiel.part2.model.Movie
import fr.upem.partiel.part2.model.Movie._
import play.api.libs.functional.syntax._
import play.api.libs.json._

object Parser {

  val directorPattern = "([A-Za-z]+) ([A-Za-z]+)".r
  val yearPattern = "([1-2][0-9][0-9][0-9])".r

  // TODO
  def toDirector: String => Option[Director] = s => {
    s match {
      case directorPattern(fn, ln) => Some(new Director {
        override def name: String = s"${fn} ${ln} "
      })
      case _ => None
    }
  }

  // TODO
  def toName: String => Title = s => new Title {
    override def name: String = s
  }

  // TODO
  def toCountry: String => Option[Country] = (s : String) => s match {
    case "FR" => Some(Country.France)
    case "UK" => Some(Country.England)
    case "GE" => Some(Country.Germany)
    case "IT" => Some(Country.Italy)
    case "US" => Some(Country.UnitedStates)
    case _ => None
  }

  // TODO
  def toYear: String => Option[Year] = s => s match {
    case yearPattern(year) => Some(new Year {
      override def year: Int = year
    })
    case _ => None
  }

  // TODO
  def toViews: BigDecimal => Option[Views] = views => {
    if (views.longValue() < 0) None else Some(new Views {
      override def views: Long = views.longValue()
    })
  }

  implicit val directorReads = Reads[Director] {
    case JsString(value) => toDirector(value).map(JsSuccess(_)).getOrElse(JsError("Not a valid Director"))
    case _ => JsError("Not a valid type for Director")
  }

  implicit val nameReads = Reads[Title] {
    case JsString(value) => JsSuccess(toName(value))
    case _ => JsError("Not a valid type for Name")
  }

  implicit val countryReads = Reads[Country] {
    case JsString(value) => toCountry(value).map(JsSuccess(_)).getOrElse(JsError("Not a valid Country"))
    case _ => JsError("Not a valid type for Country")
  }

  implicit val yearReads = Reads[Year] {
    case JsString(value) => toYear(value).map(JsSuccess(_)).getOrElse(JsError("Not a valid Year"))
    case _ => JsError("Not a valid type for Year")
  }

  implicit val viewsReads = Reads[Views] {
    case JsNumber(value) => toViews(value).map(JsSuccess(_)).getOrElse(JsError("Not a valid Views"))
    case _ => JsError("Not a valid type for Views")
  }

  implicit val movieReads: Reads[Movie] = (
    (__ \ "title").read[Title] and
      (__ \ "director").read[Director] and
      (__ \ "year").read[Year] and
      (__ \ "views").read[Views] and
      (__ \ "country").read[Country]
    ) (Movie.apply _)

}
