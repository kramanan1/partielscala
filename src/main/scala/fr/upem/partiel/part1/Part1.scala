package fr.upem.partiel.part1

import java.time.Instant
import java.time.temporal.ChronoUnit.YEARS

import scala.util.control.Exception.Finally


// Part 1 (10pts)
object Part1 {

  // 1.1 Apply 'mul2' using pattern matching to the given integer (.5pts)
  def mul2(i: Int): Int = i * 2
  def applyMul2WithPatternMatching(i: Option[Int]): Option[Int] = i match {
    case Some(value) => Some(mul2(value))
    case None => None
  }

  // 1.2 Apply 'mul2' WITHOUT using pattern matching to the given integer (.5pts)
  def applyMul2WithoutPatternMatching(i: Option[Int]): Option[Int] = if (i.isEmpty) None else Some(mul2(i.get))

  // 1.3 Refactor the following code using pattern matching (1pts)
  sealed trait Animal
  case object Cat extends Animal
  case object Bird extends Animal
  case class Dog(age: Int) extends Animal

  def formatAnimal(animal : Animal) : String = animal match {
    case Cat => "It's a cat"
    case Bird => "It's a bird"
    case Dog(age) => s"It's a ${age} year old dog"
  }


  // 1.4 Find the index of the given element if any, use recursivity (1pts)
  def indexOf[A](l: List[A], a: A): Option[Int] = l match {
    case Nil => None

    case head :: tail => {
      if (head.equals(a))
        Some(0)
      else
        indexOf(tail, a) match {
          case None => None
          case Some(v) => Some(1 + v)
        }
    }
  }


  // 1.5 Throw away all errors (.5pts)
  case class Error(message: String)
  def keepValid[A](l: List[Either[Error, A]]): List[A] = l match {
    case Nil => Nil
    case head :: tail => head match {

      case Right(e) => e :: keepValid(tail)
      case _ => keepValid(tail)
    }

  }

  // 1.6 Aggregate values (.5pts)
  def aggregate[A](l: List[A], combine: (A, A) => A, empty: A): A = l match {
    case Nil => empty
    case head :: Nil => head
    case head :: tail => combine(head, aggregate(tail, combine, empty))
  }

  // 1.7 Aggregate valid values (.5pts)
  def aggregateValid[A](l: List[Either[Error, A]], combine: (A, A) => A, empty: A): A = l match {
    case Nil =>  empty
    case head :: Nil =>  head match {
      case Right(x) => x
      case _ => empty
    }
    case head :: tail => head match {
      case Right(x) => combine(x, aggregateValid(tail, combine, empty))
      case _ => aggregateValid(tail, combine, empty)
    }
  }


  // 1.8 Create the Monoid typeclass and rewrite the above "aggregateValid" (.5pts)

  trait Monoid[A]{
    def empty : A
    def combine(x : A, y : A) : A
  }

  def aggregateValidM[A](l : List[Either[Error, A]])(implicit m : Monoid[A]) : A = l match {
    case Nil =>  m.empty
    case head :: Nil =>  head match {
      case Right(x) => x
      case _ => m.empty
    }
    case head :: tail => head match {
      case Right(x) => m.combine(x, aggregateValid(tail, m.combine, m.empty))
      case _ => aggregateValid(tail, m.combine, m.empty)
    }
  }

  // 1.9 Implement the Monoid typeclass for Strings and give an example usage with aggregateValidM (.5pts)

  object StringMonoid {
    implicit val StringMonoid = new Monoid[String] {
      override def empty: String = ""

      override def combine(x: String, y: String): String = s"${x} + ${y}"
    }


    aggregateValidM[String](List(Right("Hello"), Right("world!")))
  }





  // 1.10 Refactor the following object oriented hierarchy with an ADT (1.5pts)


  sealed trait FinancialAsset{
    def computeEarnings : Double
  }

  case class FlatRateAsset(rate : Double,  amount : Double) extends FinancialAsset {
    override def computeEarnings: Double = amount + (amount * rate)
  }

  case class LivretA(amount : Double) extends FinancialAsset{
    override def computeEarnings: Double = amount + (amount * 0.75)
  }


  case class Pel(amount : Double, creation : Instant) extends FinancialAsset{
    override def computeEarnings: Double = if (Instant.now().minus(4, YEARS).isAfter(creation))
      FlatRateAsset(1.5, amount).computeEarnings + Pel.GovernmentGrant
    else
      FlatRateAsset(1.5, amount).computeEarnings
  }

  case object Pel {
     val GovernmentGrant : Int = 1525
  }

  case class CarSale(amount : Int, horsePower : Int) extends FinancialAsset {
    override def computeEarnings: Double = amount - (CarSale.StateHorsePowerTaxation * horsePower)
  }

  object CarSale {
     val StateHorsePowerTaxation: Int = 500
  }


  // 1.11 Extract the "computeEarnings" logic of the above hierarchy
  // into an "Earnings" typeclass and create the adequate instances (1.5pts)

  trait Earnings[T] {
    def compute(t: T) : Double
  }

  object Earnings {

    implicit val financialEarnings = new Earnings[FinancialAsset] {
      override def compute(t: FinancialAsset): Double = t.computeEarnings
    }

  }

  def computeEarnings[T](t : T)(implicit ev : Earnings[T]) :Double  = ev.compute(t)


  // 1.12 Rewrite the following function with your typeclass (.5pts)
  def computeTotalEarnings(assets: List[FinancialAsset]): Double =
    assets.map(computeEarnings(_)).sum


  // 1.13 Enrich the "String" type with an "atoi" extension method that parses the
  // given String to an Int IF possible (1pts)

}
